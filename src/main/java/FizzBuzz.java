
import org.junit.Test;

    public class FizzBuzz {
        public void fizzBuzz(int n) {
            // Implementasi logika FizzBuzz di sini
            for (int i = 1; i <= n; i++) {
                if (i % 3 == 0 && i % 5 == 0) {
                    System.out.println("Fizz Buzz");
                } else if (i % 3 == 0) {
                    System.out.println("Fizz");
                } else if (i % 5 == 0) {
                    System.out.println("Buzz");
                } else {
                    System.out.println(i);
                }
            }
        }
        @Test
      public void fizzBuzz() {
            fizzBuzz(30);
        }
    }


